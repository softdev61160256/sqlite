/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.theerawen.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Windows10
 */
public class InsertUser {
    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:"+dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO user (\n"
                    + "                     id,\n"
                    + "                     username,\n"
                    + "                     password\n"
                    + "                 )\n"
                    + "                 VALUES (\n"
                    + "                     4,\n"
                    + "                     'test',\n"
                    + "                     'password'\n"
                    + "                 )";
         stmt.execute(sql);
         stmt.close();
         conn.commit();
         conn.close(); 
        } catch (ClassNotFoundException ex) {
            System.exit(0);
        } catch (SQLException ex) {     
            System.exit(0);
        }       
    }
}
