/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.theerawen.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Windows10
 */
public class SelectUser {
     public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:"+dbName);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM user");
            
            while(rs.next()){
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                
                System.out.println("ID = "+id);
                System.out.println("USERNAME = "+username);
                System.out.println("PASSWORD = "+password);
                System.out.println("");
            }
         rs.close();
         stmt.close();
         conn.close(); 
        } catch (ClassNotFoundException ex) {
            System.exit(0);
        } catch (SQLException ex) {     
            System.exit(0);
        }       
     }
}
